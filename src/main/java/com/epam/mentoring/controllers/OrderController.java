package com.epam.mentoring.controllers;

import com.epam.mentoring.models.Order;
import com.epam.mentoring.models.OrderList;
import com.epam.mentoring.services.EcommerceService;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/order")
public class OrderController {

  @Value("${jwt.header}")
  private String AUTH_HEADER;
  //@Autowired private EcommerceService ecommerceService;

  @Value("${url.order-service}")
  private String orderUrl;

  @Autowired Validator orderValidator;

  @InitBinder
  protected void initBinder(WebDataBinder binder) {
    binder.addValidators(orderValidator);
  }

  @RequestMapping(method = RequestMethod.GET)
  public List<Order> index(HttpServletRequest request) {
//    OrderList response = new RestTemplate().getForObject("http://localhost:8000/order", OrderList.class);
//    return response.getOrders();
    HttpHeaders headers = new HttpHeaders();
    headers.set(AUTH_HEADER, request.getHeader(AUTH_HEADER));
    HttpEntity entity = new HttpEntity(headers);
    ResponseEntity<Order[]> response = new RestTemplate().exchange(orderUrl, HttpMethod.GET,
            entity,Order[].class);
    return Arrays.asList(response.getBody().clone());
  }

  @PostMapping
  public Order create(@RequestBody @Valid Order order) {

    // Required by Hibernate ORM to save properly
    if (order.getItems() != null) {
      order.getItems().forEach(item -> item.setOrder(order));
    }
    HttpEntity<Order> request = new HttpEntity<>(order);
    return new RestTemplate().postForObject(orderUrl, request, Order.class);
  }

  @RequestMapping("/{id}")
  public Order view(@PathVariable("id") long id) {
    return new RestTemplate().getForObject(orderUrl + "/" + id, Order.class);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.POST)
  public Order edit(@PathVariable("id") long id, @RequestBody @Valid Order order) {
    RestTemplate restTemplate = new RestTemplate();

    Order updatedOrder = new RestTemplate().getForObject(orderUrl + "/" + id, Order.class);

    if (updatedOrder == null) {
      return null;
    }
    HttpEntity<Order> requestUpdate = new HttpEntity<>(order, new HttpHeaders());
    restTemplate.exchange(orderUrl + "/" + id, HttpMethod.PUT,
            requestUpdate, Void.class);
    return order;
  }
}
