package com.epam.mentoring.services;

import com.epam.mentoring.repositories.GroupRepository;
import com.epam.mentoring.repositories.OrderRepository;
import com.epam.mentoring.repositories.ProductRepository;
import com.epam.mentoring.models.Order;
import com.epam.mentoring.models.Product;
import com.epam.mentoring.models.ProductGroup;
import com.epam.mentoring.models.ProductImage;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class EcommerceService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    private SessionFactory sessionFactory;

    @Value("${url.order-service}")
    private String orderUrl;


    /* PRODUCT */
    public List<Product> getProducts(){
        return productRepository.findAll();
    }
    public Product getProduct(long id){
        return productRepository.findOne(id);
    }
    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    public String addProductImage(final String productId, final String filename){

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        ProductImage image = new ProductImage();
        image.setProductId(Long.parseLong(productId));
        image.setPath(filename);

        try {
            String res = session.save(image).toString();
            session.getTransaction().commit();
            return res;
        } catch (HibernateException e) {
            System.out.print(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
        return "";
    }

    /* GROUPS */
    public List<ProductGroup> getGroups(){
        return groupRepository.findAll();
    }
    public ProductGroup getGroup(long id){
        return groupRepository.findOne(id);
    }
    public ProductGroup saveGroup(ProductGroup group){
        return groupRepository.save(group);
    }

    /* ORDERS */
    public List<Order> getOrders(){
        ResponseEntity<Order[]> response = new RestTemplate().getForEntity(orderUrl, Order[].class);
        return Arrays.asList(response.getBody().clone());
    }
    public Order getOrder(long id){
        return new RestTemplate().getForObject(orderUrl + "/" + id, Order.class);
    }
    public Order saveOrder(Order order){
        HttpEntity<Order> request = new HttpEntity<>(order);
        return new RestTemplate().postForObject(orderUrl, request, Order.class);
    }
}
